# Ubuntu Touch app development course

This course teaches you [how to develop an app for Ubuntu Touch with Clickable](https://ubports.gitlab.io/marketing/education/ub-clickable-1/).

## How to contribute

Contributions to this course are welcome. The source documents are written in [AsciiDoc](https://asciidoc.org).

You can generate the HTML documentation from the source documents with:

```shell
./scripts/generate_html.sh
```

Then open the index file in `public/index.html`.

You can generate  a PDF from the source documents with:

```shell
./scripts/generate_pdf.sh
```

For this to work you need a couple of programs:
- asciidoctor-pdf. Yes, we prefer the Ruby version over the original Python program, for speed and features richness.
- gs from GhostScript to convert pdf to pdf with embeddded fonts. 
- identify and convert from ImageMagick to remove transparancy from image files as POD site Lulu.com does not like those in manuscripts.
- Optionally pdffonts from poppler-utils to see embedded fonts in pdf files.

This generates a trainingbook.pdf and also a trainingbook-prepress.pdf.
The latter has embedded fonts and is mandatory on printing-on-demand sites like Lulu.com


## License

[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)

The course text has been written by Koen Vervloesem, based on input and [code](https://github.com/codefounders-nl/ubtouch-shoppinglist) by Felix van Loenen, Terence Sambo, Leandro d'Agostino and Sander Klootwijk.
