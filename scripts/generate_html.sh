#!/usr/bin/env bash
for adoc in *.adoc; do
    asciidoctor -D public --doctype=book "$adoc"
done
cp -r images public
